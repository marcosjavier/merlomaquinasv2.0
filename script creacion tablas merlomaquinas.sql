CREATE DATABASE `merlomaquinas` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;

CREATE TABLE `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` blob,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `marca` (
  `idMarca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` blob,
  PRIMARY KEY (`idMarca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `menu` (
  `idItem` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `referencia` varchar(70) COLLATE utf8_spanish_ci DEFAULT NULL,
  `target` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `inView` int(11) DEFAULT NULL,
  PRIMARY KEY (`idItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` blob,
  `pathImgPrincipal` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idMarca` int(11) DEFAULT NULL,
  `idCategoria` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

CREATE TABLE `imagen` (
  `idImagen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` blob,
  `path` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idProducto` int(11) DEFAULT NULL,
  PRIMARY KEY (`idImagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE Producto
ADD FOREIGN KEY (idMarca) REFERENCES Marca(idMarca);

ALTER TABLE Producto
ADD FOREIGN KEY (idCategoria) REFERENCES Categoria(idCategoria);

ALTER TABLE Imagen
ADD FOREIGN KEY (idProducto) REFERENCES Producto(idProducto);















