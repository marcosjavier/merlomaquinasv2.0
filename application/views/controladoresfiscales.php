<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

      <title class="bg-primary">MERLO MAQUINAS</title>
    <!-- Bootstrap core CSS -->
    <link href=<?php echo "'" . base_url()?>vendor/bootstrap/css/bootstrap.min.css<?php echo "'"?> rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=<?php echo "'" . base_url()?>css/modern-business.css<?php echo "'"?> rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html">MERLO MAQUINAS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.html">Bienvenidos</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Producto
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
			  <a class="dropdown-item" href="controladoresfiscales.html">Controladores Fiscales </a>
                <a class="dropdown-item" href="portfolio-1-col.html">1 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-2-col.html">2 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-3-col.html">3 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-4-col.html">4 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-item.html">Single Portfolio Item</a>
				<a class="dropdown-item" href="insumos.html">Insumos / Accesorios</a>
              </div>
            </li>
			 <li class="nav-item">
              <a class="nav-link" href="catalogo.html">Catalogo</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="Museo Tecnologico.html">Museo Tecnologico</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contacto.html">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"> Controladores Fiscales
 
 
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Productos</li>
      </ol>

      <div class="row">
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/ER 420F - Controlador Fiscal.jpg"alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="ER420F.html">ER 420F - Controlador Fiscal</a>
              </h4>
              <p class="ER420F.html">Excelente relación costo/beneficio. Ideal para todo tipo de comercios. Conectividad a pc, balanzas, scanners mediante RS232. Impresor térmico de alta velocidad.</p>

            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="img/ER 420F - Controlador Fiscal.jpg" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="MORETTICR35.html">MORETTI CR35</a>
              </h4>
              <p class="card-text">Impresor Térmico de alta velocidad. Excelente relación costo-beneficio. 3000 PLU's de capacidad. Conexión a Scanner, Balanza y PC. Diseño moderno, dimensiones reducidas, diseño ergonómico, doble display LCD retroiluminado.</p>
            </div>
          </div>
        </div>
      </div>

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">MyM Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=<?php echo "'" . base_url()?>vendor/jquery/jquery.min.js<?php echo "'"?>></script>
    <script src=<?php echo "'" . base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js<?php echo "'"?>></script>

  </body>

</html>
