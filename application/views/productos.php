<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MERLO MAQUINAS</title>

    <!-- Bootstrap core CSS -->
    <link href=<?php echo "'" . base_url()?>vendor/bootstrap/css/bootstrap.min.css<?php echo "'"?> rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href=<?php echo "'" . base_url()?>css/modern-business.css<?php echo "'"?> rel="stylesheet">
	    <!-- Custom Fonts -->
    <link href=<?php echo "'" . base_url()?>vendor/font-awesome/css/font-awesome.min.css<?php echo "'"?> rel="stylesheet" type="text/css">

  </head>

  <body>
    <?php include("navbar.php");?>
    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Productos</h1>
	   <div class="navbar-default sidebar" role="navigation">
		<ul class="nav" id="side-menu">
	           <li class="sidebar-search col-md-3">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Buscar producto...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
				</li>
				<li class="sidebar-search col-md-3">
						    <div class="form-group">
                                            <select class="form-control">
                                                <option>Seleccionar Producto</option>
                                                <option href=<?php echo "'" . base_url()?>prod1.html">Impresoras fiscales</option>
                                                <option href=<?php echo "'" . base_url()?>prod2.html">Impresoras de Comandas o No Fiscales</option>
                                                <option href=<?php echo "'" . base_url()?>prod3.html">Controladores Fiscales</option>
                                                <option href=<?php echo "'" . base_url()?>prod4.html">Contadoras de Billetes</option>
												<option href=<?php echo "'" . base_url()?>prod5.html">Balanzas Industriales</option>
												<option href=<?php echo "'" . base_url()?>prod6.html">Balanzas de Precisión</option>
												<option href=<?php echo "'" . base_url()?>prod7.html">Calculadoras para Oficinas/Comercio</option>
												<option href=<?php echo "'" . base_url()?>prod8.html">Lectores de código de Barras</option>
												<option href=<?php echo "'" . base_url()?>prod9.html">Lectores de cheques</option>
												<option href=<?php echo "'" . base_url()?>prod10.html">Destructores de Papeles</option>
												<option href=<?php echo "'" . base_url()?>prod11.html">Gavetas para Dinero</option>
												<option href=<?php echo "'" . base_url()?>prod12.html">Conectividad/Router/placa de video/Accesorios</option>
												<option href=<?php echo "'" . base_url()?>prod13.html">Sillas</option>
                                            </select>
                                        </div>
				</li>
						</br></br></br>
	     </ul>
		 </div>
	  
	  
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod1.html">Impresoras fiscales</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod2.html">Impresoras de Comandas o No Fiscales</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
 
		     <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod3.html">Controladores Fiscales</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod4.html">Contadoras de Billetes</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod5.html">Balanzas Industriales</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod6.html">Balanzas de Precisión</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque earum nostrum suscipit ducimus nihil provident, perferendis rem illo, voluptate atque, sit eius in voluptates, nemo repellat fugiat excepturi! Nemo, esse.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod7.html">Calculadoras para Oficinas/Comercio </a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod8.html">Lectores de código de Barras</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius adipisci dicta dignissimos neque animi ea, veritatis, provident hic consequatur ut esse! Commodi ea consequatur accusantium, beatae qui deserunt tenetur ipsa.</p>
            </div>
          </div>
        </div>
		  <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod9.html">Lectores de cheques</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
	   <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod10.html">Destructores de Papeles</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
	   <div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod11.html">Gavetas para Dinero</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod12.html">Conectividad/Router/placa de video/Accesorios</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
		<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item">
          <div class="card h-100">
            <a href=<?php echo "'" . base_url()?>#"><img class="card-img-top" src=<?php echo "'" . base_url()?>http://placehold.it/700x400" alt=""></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href=<?php echo "'" . base_url()?>prod13.html">Sillas</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
      </div>

      <!-- Pagination -->
      <ul class="pagination justify-content-center">
        <li class="page-item">
          <a class="page-link" href=<?php echo "'" . base_url()?>#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item">
          <a class="page-link" href=<?php echo "'" . base_url()?>#">1</a>
        </li>
     
        <li class="page-item">
          <a class="page-link" href=<?php echo "'" . base_url()?>#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>

    </div>
    <!-- /.container -->

    <!-- Footer -->
	<footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white"> MyM Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src=<?php echo "'" . base_url()?>vendor/jquery/jquery.min.js"></script>
    <script src=<?php echo "'" . base_url()?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
